﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    [SerializeField] private Engine engine;
    [SerializeField] private Transform[] checkRays;
    [SerializeField] private float checkDistance = 3f;
    [SerializeField] private float safeDistance = 1f;
    [SerializeField] private LayerMask obstacleMask;

    [SerializeField] private Transform carrot;
    [SerializeField] private float scavengeRadius = 150f;
    [SerializeField] private float goalCompleteRadius = 8f;
    [SerializeField] private float reroutePeriod = 1f;

    [SerializeField] private bool correctoinDebug = false;
    [SerializeField] private bool destinationDebug = false;

    private Vector3 destination;

    //[SerializeField] private float safePassage = 5f;
    //private bool virtualObstacle = false;
    //private Vector3 o0 = Vector3.zero;
    //private float r0 = 0f;

    void Start()
    {
        engine.SetMode(true);
        StartCoroutine(RandomTarget());
    }

    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (carrot != null) destination = carrot.position;
        engine.SetTarget(destination);
        CourseCorrection();
    }

    private void CourseCorrection()
    {
        CircleCollider2D obstaclePrimary = null;
        //CircleCollider2D obstacleSecondary = null;
        var distance = checkDistance;
        for (var i = 0; i < checkRays.Length; i++)
        {
            var hit = Physics2D.Raycast(checkRays[i].position, checkRays[i].up, checkDistance, obstacleMask);
            if (hit)
            {
                var col = (CircleCollider2D)hit.collider;
                if (hit.distance < distance)
                {
                    distance = hit.distance;
                    //obstacleSecondary = obstaclePrimary;
                    obstaclePrimary = col;
                }
                //else
                //{
                //    obstacleSecondary = col;
                //}

                if (correctoinDebug) Debug.DrawLine(checkRays[i].position, hit.point, Color.magenta);
            }
            else
            {
                if (correctoinDebug) Debug.DrawLine(checkRays[i].position, checkRays[i].position + checkRays[i].up * checkDistance, Color.cyan);
            }
        }

        if (destinationDebug) Debug.DrawLine(transform.position, destination, Color.green);

        if (obstaclePrimary == null)
        {
            //virtualObstacle = false;
            return;
        }

        //if (obstacleSecondary == null) obstacleSecondary = obstaclePrimary;

        //var o1 = obstaclePrimary.transform.position;
        //var o2 = obstacleSecondary.transform.position;
        //var r1 = obstaclePrimary.radius * obstaclePrimary.transform.lossyScale.x;
        //var r2 = obstacleSecondary.radius * obstacleSecondary.transform.lossyScale.x;

        //if (Vector3.Distance(o1, o0) + r1 > r0)
        //{
        //    var v = o0 - o1;
        //    var dir = v.normalized;
        //    var d = v.magnitude;
        //    //o0 = o1 + dir * 0.5f * (d + r0 - r1);
        //    //r0 = 0.5f * (r1 + r0 + d);
        //    r0 = d + r1;
        //}

        //if (Vector3.Distance(o2, o0) + r2 > r0)
        //{
        //    var v = o0 - o2;
        //    var dir = v.normalized;
        //    var d = v.magnitude;
        //    //o0 = o2 + dir * 0.5f * (d + r0 - r2);
        //    //r0 = 0.5f * (r2 + r0 + d);
        //    r0 = d + r2;
        //}

        var o = obstaclePrimary.transform.position;
        var r = obstaclePrimary.radius * obstaclePrimary.transform.lossyScale.x;

        //if (virtualObstacle)
        //{
        //    o = o0;
        //    r = r0;
        //}
        //else if (obstaclePrimary == obstacleSecondary)
        //{
        //    o = obstaclePrimary.transform.position;
        //    r = obstaclePrimary.radius * obstaclePrimary.transform.lossyScale.x;
        //}
        //else
        //{
        //    var v = o2 - o1;
        //    var dir = v.normalized;
        //    var d = v.magnitude;

        //    if (d - r1 - r2 < safePassage)
        //    {
        //        o = o1 + dir * 0.5f * (d + r2 - r1);
        //        r = 0.5f * (r1 + r2 + d);

        //        virtualObstacle = true;
        //        o0 = o;
        //        r0 = r;
        //    }
        //    else
        //    {
        //        o = o1;
        //        r = r1;
        //    }
        //}

        
        var dirToObs = o - transform.position;

        var d2o = destination - o;
        if (d2o.magnitude < r + safeDistance) destination = o + d2o.normalized * (r + safeDistance);

        if (dirToObs.sqrMagnitude < r * r)
        {
            var exitPoint = o + (-dirToObs).normalized * (r + safeDistance);
            engine.SetTarget(exitPoint);

            if (correctoinDebug) Debug.DrawLine(transform.position, exitPoint, Color.red);

            return;
        }

        var beta = Mathf.Rad2Deg * Mathf.Acos(r / dirToObs.magnitude);
        var tp1 = o + Quaternion.Euler(0, 0, beta) * (-dirToObs).normalized * (r + safeDistance);
        var tp2 = o + Quaternion.Euler(0, 0, -beta) * (-dirToObs).normalized * (r + safeDistance);
        var tangentPoint = Vector3.Angle(transform.up, tp1 - transform.position) < Vector3.Angle(transform.up, tp2 - transform.position) ? tp1 : tp2;

        if (Mathf.Sign(Vector3.SignedAngle(transform.up, destination - transform.position, Vector3.forward)) ==
            Mathf.Sign(Vector3.SignedAngle(transform.up, o - transform.position, Vector3.forward)))
        {
            engine.SetTarget(tangentPoint);
            if (correctoinDebug) Debug.DrawLine(transform.position, tangentPoint, Color.yellow);
        }
        else if (Vector3.Angle(tangentPoint - transform.position, transform.up)> Vector3.Angle(transform.up, destination - transform.position))
        {
            engine.SetTarget(tangentPoint);
            if (correctoinDebug) Debug.DrawLine(transform.position, tangentPoint, Color.yellow);
        }
        else
        {
            if (correctoinDebug) Debug.DrawLine(transform.position, tangentPoint, Color.white);
        }
    }

    IEnumerator RandomTarget()
    {
        while (true)
        {
            //destination = Random.insideUnitCircle * 20f;
            //yield return new WaitForSeconds(Random.Range(1f, 5f));

            if (Vector3.Distance(destination, transform.position) < goalCompleteRadius)
            {
                destination = Random.insideUnitCircle * scavengeRadius;
            }
            yield return new WaitForSeconds(reroutePeriod);
        }
    }

    //private void OnDrawGizmos()
    //{
    //    if (virtualObstacle) Gizmos.DrawWireSphere(o0, r0);
    //}
}
