﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpaceShip3D : MonoBehaviour
{
    [SerializeField] private Engine3D engine;

    private Camera cam;
    private bool engineStoped = true;

    void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            engineStoped = !engineStoped;
            engine.SetMode(!engineStoped);
        }
        if (!engineStoped)
        {
            if (Input.GetMouseButtonDown(1)) engine.SetMode(false);
            else if (Input.GetMouseButtonUp(1)) engine.SetMode(true);

            var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
            mousePos.y = 0f;
            engine.SetTarget(mousePos);
        }
        
    }
}
