﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public enum FollowMode
    {
        Instant,
        Smooth
    }

    [SerializeField] private Transform target;
    [SerializeField] private FollowMode mode = FollowMode.Smooth;
    [SerializeField] private float smoothing = 0.1f;

    private Vector3 currentVelocity; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (mode)
        {
            case FollowMode.Instant:
                transform.position = target.position;
                break;
            case FollowMode.Smooth:
                transform.position = Vector3.SmoothDamp(transform.position, target.position, ref currentVelocity, smoothing);
                break;
            default:
                break;
        }

    }
}
