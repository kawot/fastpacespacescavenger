﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpaceShip : MonoBehaviour
{
    [SerializeField] private Engine engine;

    private Camera cam;
    private bool engineStoped = true;

    void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            engineStoped = !engineStoped;
            engine.SetMode(!engineStoped);
        }
        if (!engineStoped)
        {
            if (Input.GetMouseButtonDown(1)) engine.SetMode(false);
            else if (Input.GetMouseButtonUp(1)) engine.SetMode(true);

            var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0f;
            engine.SetTarget(mousePos);
        }
        
    }
}
