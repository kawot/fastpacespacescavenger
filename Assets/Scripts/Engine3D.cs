﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Engine3D : MonoBehaviour
{
    [SerializeField] private Transform hull;

    [SerializeField] private float turnRate = 180f;
    [SerializeField] private float maxSpeed = 20f;
    [SerializeField] private float engineForce = 1f;

    [SerializeField] private Transform carrot;
    [SerializeField] private float carrotDistance = 1f;
    [SerializeField] private TextMeshProUGUI speedLabel;

    //private EngineMode mode = EngineMode.Off;
    private float thrust = 0f;
    private Vector3 target = Vector3.zero;
    private Vector3 velocity = Vector3.zero;
    private float drag = 0f;

    public enum Mode
    {
        On,
        Off
    }

    public void SetTarget(Vector3 destination)
    {
        target = destination;
    }

    public void SetMode(bool start)
    {
        if (start) SetMode(Mode.On);
        else SetMode(Mode.Off);
    }

    public void SetMode(Mode mode)
    {
        switch (mode)
        {
            case Mode.On:
                thrust = 1f;
                break;
            case Mode.Off:
                thrust = 0f;
                break;
            default:
                break;
        }
    }

    private void FixedUpdate()
    {
        var dir = target - hull.position;

        var turnDelta = Mathf.Clamp(Vector3.SignedAngle(hull.forward, dir, Vector3.up), -turnRate * Time.fixedDeltaTime, turnRate * Time.fixedDeltaTime);
        hull.Rotate(Vector3.up * turnDelta);

        velocity += hull.forward * thrust * engineForce * 50f * Time.fixedDeltaTime;
        hull.Translate(velocity * Time.fixedDeltaTime, Space.World);

        drag = engineForce / (engineForce + maxSpeed);
        velocity *= (1f - drag);

        if (carrot != null) carrot.position = hull.position + velocity * carrotDistance;
        if (speedLabel != null) speedLabel.text = (0.1f * Mathf.RoundToInt(velocity.magnitude * 10f)).ToString();
    }
}
