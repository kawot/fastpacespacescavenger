﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI3D : MonoBehaviour
{
    [SerializeField] private Engine3D engine;
    [SerializeField] private float checkDistance = 3f;
    [SerializeField] private float safeDistance = 1f;

    [SerializeField] private Transform target;
    [SerializeField] private NavMeshAgent carrot;
    [SerializeField] private float carrotLash = 5f;
    [SerializeField] private float scavengeRadius = 150f;
    [SerializeField] private float goalCompleteRadius = 8f;
    [SerializeField] private float reroutePeriod = 1f;

    [SerializeField] private bool destinationDebug = false;
    [SerializeField] private bool correctionDebug = true;

    private Vector3 destination;

    void Start()
    {
        engine.SetMode(true);
        StartCoroutine(RandomTarget());
    }

    void FixedUpdate()
    {
        if (target != null) destination = target.position;
        carrot.destination = destination;

        var carrotPos = carrot.transform.position;
        carrotPos.y = 0f;
        var dir = carrotPos - transform.position;
        if (dir.magnitude > carrotLash) carrot.transform.position = transform.position + dir.normalized * carrotLash;
        engine.SetTarget(carrotPos);
        //CourseCorrection();

        if (destinationDebug) Debug.DrawLine(transform.position, destination, Color.green);
    }

    private void CourseCorrection()
    {
        SphereCollider obstacle = null;
        var c = carrot.transform.position;
        c.y = 0f;

        RaycastHit hit;
        if (Physics.Raycast(c, carrot.transform.forward, out hit, checkDistance))
        {
            obstacle = (SphereCollider)hit.collider;

            if (correctionDebug) Debug.DrawLine(c, hit.point, Color.magenta);
        }
        else
        {
            if (correctionDebug) Debug.DrawLine(c, c + carrot.transform.forward * checkDistance, Color.cyan);
        }

        if (obstacle == null) return;

        var o = obstacle.transform.position;
        var r = obstacle.radius * obstacle.transform.lossyScale.x;

        var v = c - o;

        Debug.Log(v.magnitude);

        if (v.magnitude < r + safeDistance)
        {
            carrot.transform.position = o + v * (r + safeDistance);
        }
    }

        IEnumerator RandomTarget()
    {
        while (true)
        {
            if (Vector3.Distance(destination, transform.position) < goalCompleteRadius)
            {
                var rndCircle = Random.insideUnitCircle * scavengeRadius;
                destination = new Vector3(rndCircle.x, 0f, rndCircle.y);
                //var counter = 0;
                //while (counter < 100 && Vector3.Angle(destination - transform.position, transform.forward) > 60f)
                //{
                //    rndCircle = Random.insideUnitCircle * scavengeRadius;
                //    destination = new Vector3(rndCircle.x, 0f, rndCircle.y);
                //    counter++;
                //}
                //Debug.Log(counter);
            }
            yield return new WaitForSeconds(reroutePeriod);
        }
    }
}
